package com.fragment.kevinalejandrocastano.maps.activities;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.fragment.kevinalejandrocastano.maps.R;
import com.fragment.kevinalejandrocastano.maps.fragments.MapFragment;
import com.fragment.kevinalejandrocastano.maps.fragments.WelcomeFragment;


public class MainActivity extends AppCompatActivity {


    Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*Toast.makeText(this, "Called", Toast.LENGTH_SHORT).show();*/
        /*This condition allows me to rotate the device to show me the map and not restart the activity again*/
        if (savedInstanceState == null){
            currentFragment = new WelcomeFragment();
            changeFragment(currentFragment);
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_welcome:
               currentFragment = new WelcomeFragment();
                break;
            case  R.id.menu_maps:
                currentFragment = new MapFragment();
                break;
        }
         changeFragment(currentFragment);
        return super.onOptionsItemSelected(item);
    }

    private void changeFragment (Fragment fragment) {

        //getFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();

    }
}
